// Ipsmon monitors and manages the Inductive Power (charging) System on the Deep
// Profiler dock.
package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	"log/slog"

	"bitbucket.org/uwaploe/ipsmon/internal/ips"
	"bitbucket.org/uwaploe/ipsmon/internal/rdsub"
	"bitbucket.org/uwaploe/ipsmon/internal/wlan"
	"github.com/gomodule/redigo/redis"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const Usage = `Usage: ipsmon [options] cfgfile

Monitor the DP inductive power system and enable/disable based on
rules in cfgfile.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rpcAddr       string = "localhost:10101"
	rdAddr        string = "localhost:6379"
	rdChan        string = "data.eng"
	minLevel      int    = -30
	disableOnExit bool
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rpcAddr, "rpc-addr", rpcAddr,
		"host:port for the TS-FPGA gRPC server")
	flag.StringVar(&rdAddr, "rd-addr", rdAddr,
		"host:port for the Redis server")
	flag.StringVar(&rdChan, "chan", rdChan,
		"Redis pub-sub channel for battery data")
	flag.BoolVar(&disableOnExit, "safe", disableOnExit,
		"Disable the IPS on exit")
	flag.IntVar(&minLevel, "level", minLevel,
		"Minimum WiFi signal level to determine docking (dBm)")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func grpcConnect(addr string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	opts = append(opts, grpc.WithBlock())

	return grpc.Dial(addr, opts...)
}

func isEnabled(charger *ips.Ips) bool {
	mask := ips.BiasEnabled | ips.ChargingEnabled
	status, _ := charger.Status()
	return (status & mask) == mask
}

// Check that the vehicle is in the dock by checking the WiFi signal
// strength.
func inDock(level int) bool {
	stats, err := wlan.GetStats("wlan0")
	if err != nil {
		return false
	}
	return stats.Level > level
}

func processRecord(rec Record, charger *ips.Ips, cfg sysConfig) {

	for _, bcode := range cfg.Enable {
		state, err := runByteCode(bcode, rec.Data)
		if err != nil {
			continue
		}
		if state && !isEnabled(charger) {
			slog.Info("enabling IPS", slog.String("rule", bcode.Source().String()))
			if err := charger.Enable(); err != nil {
				slog.Error("", slog.Any("err", err))
			}
		}
	}

	for _, bcode := range cfg.Disable {
		state, err := runByteCode(bcode, rec.Data)
		if err != nil {
			continue
		}
		if state && isEnabled(charger) {
			slog.Info("disabling IPS", slog.String("rule", bcode.Source().String()))
			if err := charger.Disable(); err != nil {
				slog.Error("", slog.Any("err", err))
			}
		}
	}

}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		fmt.Fprintln(os.Stderr, "Missing configuration file")
		flag.Usage()
		os.Exit(1)
	}

	logger := initLogger(false)
	slog.SetDefault(logger)

	cfg, err := loadConfig(args[0])
	if err != nil {
		abort("read configuration", err, slog.String("path", args[0]))
	}

	conn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		abort("connect to Redis server", err, slog.String("addr", rdAddr))
	}
	defer conn.Close()
	psc := redis.PubSubConn{Conn: conn}

	gconn, err := grpcConnect(rpcAddr)
	if err != nil {
		abort("connect to TS-FPGA server", err, slog.String("addr", rpcAddr))
	}
	defer gconn.Close()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ch := make(chan Record, 1)
	errCh := make(chan error, 1)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		errCh <- rdsub.ReceiveMsgs(ctx, psc, func(m redis.Message) bool {
			rec := Record{}
			if err := json.Unmarshal(m.Data, &rec); err == nil {
				select {
				case ch <- rec:
				default:
				}
			}
			return false
		}, logger)
	}()

	charger := ips.New(gconn)
	psc.Subscribe(rdChan)
	defer func() {
		_ = psc.Unsubscribe()
		if disableOnExit {
			slog.Info("Disabling IPS")
			charger.Disable()
		}
	}()

	slog.Info("IPS monitor starting", slog.String("vers", Version))
	for {
		select {
		case rec := <-ch:
			processRecord(rec, charger, cfg)
		case err := <-errCh:
			log.Printf("error: %v", err)
			slog.Error("", slog.Any("err", err))
			return
		case sig := <-sigs:
			slog.Info("got signal", slog.String("sig", sig.String()))
			cancel()
			return
		}
	}

}
