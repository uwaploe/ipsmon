package wlan

import (
	"reflect"
	"strings"
	"testing"
)

const procOutput = `Inter-| sta-|   Quality        |   Discarded packets               | Missed | WE
 face | tus | link level noise |  nwid  crypt   frag  retry   misc | beacon | 22
 wlan0: 0000  100.  -19   -83        0      0      0      0      0        0
`

func TestRead(t *testing.T) {
	expect := Entry{
		IfName: "wlan0",
		Status: 0,
		Qual:   100,
		Level:  -19,
		Noise:  -83,
	}
	rdr := strings.NewReader(procOutput)
	e, err := readEntry(rdr, "wlan0")
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(e, expect) {
		t.Errorf("Parsing error; expected %#v, got %#v", expect, e)
	}
}
