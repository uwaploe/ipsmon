package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/expr-lang/expr"
	"github.com/expr-lang/expr/vm"
)

var ErrInvalid = errors.New("Invalid expression")

type Env struct {
	Current      int `json:"current" expr:"current"`
	Voltage      int `json:"voltage" expr:"voltage"`
	Vmon         int `json:"vmon" expr:"vmon"`
	AmbientTemp  int `json:"T_ambient" expr:"t_ambient"`
	HeatsinkTemp int `json:"T_heatsink" expr:"t_heatsink"`
}

func (Env) IsDocked() bool {
	return inDock(minLevel)
}

func (e Env) IsCharging() bool {
	return e.Current > 0
}

func genByteCode(code string) (*vm.Program, error) {
	bcode, err := expr.Compile(code, expr.Env(Env{}), expr.AsBool())
	if err != nil {
		err = fmt.Errorf("%q: %v: %w", code, err, ErrInvalid)
	}
	return bcode, err
}

func runByteCode(bcode *vm.Program, env Env) (bool, error) {
	output, err := expr.Run(bcode, env)
	if err != nil {
		return false, fmt.Errorf("%q: %w", bcode.Source().String(), err)
	}

	return output.(bool), nil
}

type Record struct {
	T    time.Time
	Data Env
}

func updateInt(m map[string]interface{}, key string, val *int) {
	if v, ok := m[key]; ok {
		switch x := v.(type) {
		case float64:
			*val = int(x)
		case int64:
			*val = int(x)
		}
	}
}

func (r *Record) UnmarshalJSON(b []byte) error {
	val := struct {
		Src  string                 `json:"name"`
		T    [2]int64               `json:"t"`
		Data map[string]interface{} `json:"data"`
	}{}

	if err := json.Unmarshal(b, &val); err != nil {
		return err
	}

	r.T = time.Unix(val.T[0], val.T[1]*1e3).UTC()

	updateInt(val.Data, "current", &r.Data.Current)
	updateInt(val.Data, "voltage", &r.Data.Voltage)
	updateInt(val.Data, "vmon", &r.Data.Vmon)
	updateInt(val.Data, "T_ambient", &r.Data.AmbientTemp)
	updateInt(val.Data, "T_heatsink", &r.Data.HeatsinkTemp)

	return nil
}
