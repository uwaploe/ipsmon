package main

import (
	"os"

	"github.com/expr-lang/expr/vm"
	yaml "gopkg.in/yaml.v3"
)

type sysConfig struct {
	Enable  []*vm.Program
	Disable []*vm.Program
}

func (c *sysConfig) UnmarshalYAML(node *yaml.Node) error {
	val := struct {
		Enable  []string `yaml:"enable"`
		Disable []string `yaml:"disable"`
	}{}
	if err := node.Decode(&val); err != nil {
		return err
	}

	c.Enable = make([]*vm.Program, len(val.Enable))
	for i, code := range val.Enable {
		bcode, err := genByteCode(code)
		if err != nil {
			return err
		}
		c.Enable[i] = bcode
	}

	c.Disable = make([]*vm.Program, len(val.Disable))
	for i, code := range val.Disable {
		bcode, err := genByteCode(code)
		if err != nil {
			return err
		}
		c.Disable[i] = bcode
	}

	return nil
}

func loadConfig(filename string) (sysConfig, error) {
	var cfg sysConfig
	contents, err := os.ReadFile(filename)
	if err != nil {
		return cfg, err
	}

	err = yaml.Unmarshal(contents, &cfg)
	return cfg, err
}
