package main

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"
)

func testValid(t *testing.T, in string, expected bool) {
	_, err := genByteCode(in)
	if result := (err == nil); expected != result {
		t.Errorf("Check failed; expected %v, got %v", expected, result)
	}
}

func testValue(t *testing.T, syms Env, in string, expected bool) {
	bcode, err := genByteCode(in)
	if err != nil {
		t.Fatal(err)
	}

	result, err := runByteCode(bcode, syms)
	if err != nil {
		t.Error(err)
	}
	if result != expected {
		t.Errorf("Check failed; expected %v, got %v", expected, result)
	}
}

func TestValidExpr(t *testing.T) {
	tests := map[string]struct {
		in  string
		out bool
	}{
		"bad-var":    {in: "foo == 42", out: false},
		"valid":      {in: "current > 6000", out: true},
		"dock-check": {in: "current > 6000 && IsDocked()", out: true},
		"invalid":    {in: "current ==> 6000", out: false},
		"multi-term": {in: "current > 6000 && t_ambient > 20000", out: true},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			testValid(t, tc.in, tc.out)
		})
	}
}

func TestEvalBool(t *testing.T) {
	syms := Env{
		Voltage:      16234,
		Current:      5123,
		AmbientTemp:  10500,
		HeatsinkTemp: 14000,
	}

	tests := map[string]struct {
		in  string
		out bool
	}{
		"voltage-1":  {in: "voltage > 16200", out: true},
		"current-1":  {in: "current > 7000", out: false},
		"multi-term": {in: "current > 7000 || t_heatsink > 13500", out: true},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			testValue(t, syms, tc.in, tc.out)
		})
	}
}

func TestLoadConfig(t *testing.T) {
	_, err := loadConfig("config/ipsmon.yaml")
	if err != nil {
		t.Fatal(err)
	}
}

func TestReadRecord(t *testing.T) {
	dr0 := dataRecord{Src: "profiler", T: [2]int64{0, 0},
		Data: map[string]interface{}{
			"current": 0, "humidity": 314, "itemp": []int{310, 0},
			"voltage": 15638, "vmon": 15640}}
	dr1 := dataRecord{Src: "dock", T: [2]int64{30, 100000},
		Data: map[string]interface{}{"T_ambient": 22797,
			"T_heatsink": 22212, "humidity": 310}}
	expected := Record{
		T: time.Unix(30, 100000*1000).UTC(),
		Data: Env{
			Voltage:      15638,
			Vmon:         15640,
			AmbientTemp:  22797,
			HeatsinkTemp: 22212}}

	var rec Record

	b, err := json.Marshal(dr0)
	if err != nil {
		t.Fatal(err)
	}
	err = json.Unmarshal(b, &rec)
	if err != nil {
		t.Fatal(err)
	}

	b, err = json.Marshal(dr1)
	if err != nil {
		t.Fatal(err)
	}

	err = json.Unmarshal(b, &rec)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(rec, expected) {
		t.Errorf("bad value; expected %#v, got %#v", expected, rec)
	}
}
