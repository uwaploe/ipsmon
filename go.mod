module bitbucket.org/uwaploe/ipsmon

go 1.20

require (
	bitbucket.org/uwaploe/tsfpga v0.7.1
	github.com/alicebob/miniredis/v2 v2.33.0
	github.com/expr-lang/expr v1.16.9
	github.com/gomodule/redigo v1.8.9
	golang.org/x/exp v0.0.0-20240707233637-46b078467d37
	google.golang.org/grpc v1.45.0
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)

require (
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/yuin/gopher-lua v1.1.1 // indirect
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd // indirect
	golang.org/x/text v0.3.0 // indirect
	google.golang.org/genproto v0.0.0-20200526211855-cb27e3aa2013 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
