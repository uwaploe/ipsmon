# Deep Profiler IPS Monitor

Ipsmon is a Runit service which monitors the inductive charging system on the Deep Profiler Dock. A configuration file sets the criteria for enabling and disabling the IPS.

## Installation

Fetch the latest compressed TAR archive from the [Downloads](https://bitbucket.org/uwaploe/ipsmon/downloads/) section of this repository and copy to `~rsn/stow` directory on the DSC, unpack and install as shown below (replace 0.2.1 with the program version):

``` shellsession
$ cd stow
$ tar -x -z -f ipsmon_0.2.1_linux_armv5.tar.gz
$ cd ipsmon_0.2.1_linux_armv5
$ ./scripts/install.sh
tsfpga is /usr/local/bin/tsfpga
`/home/rsn/bin/ipsmon' -> `/home/rsn/stow/ipsmon_0.2.1_linux_armv5/ipsmon'
`runit/ipsmon/log/run' -> `/home/rsn/sv/ipsmon/log/run'
`runit/ipsmon/run' -> `/home/rsn/sv/ipsmon/run'
`config/ipsmon.yaml' -> `/home/rsn/config/ipsmon.yaml' (backup: `/home/rsn/config/ipsmon.yaml.~3~')
```

After making any needed edits to the configuration file (see next section), restart the Runit service script from the services directory:

``` shellsession
$ sv restart ./ipsmon
ok: run: ./ipsmon: (pid 17437) 1s
```

## Configuration File

The installation script copies the configuration file to `~/config/ipsmon.yaml` after making a numbered backup copy of any existing version (e.g. `~/config/ipsmon.yaml.~2~`).

``` yaml
---
# IPS monitor threshold settings. Voltage values are in mV,
# current values in mA, and temperature values are in mdegC

# Conditions for enabling the IPS
enable:
  - "voltage < 15000"
# Conditions for disabling the IPS
disable:
  - "voltage > 16100"
  - "current > 6000"
  - "t_ambient > 19000"
```

The enable and disable conditions are a series of logical statements about the values of the following variables:

  - **voltage**: vehicle battery voltage (mV)
  - **current**: vehicle battery current (mA)
  - **vmon**: vehicle voltage monitor reading (mV)
  - **t_ambient**: dock ambient internal temperature (millidegrees C)
  - **t_heatsink**: dock heat-sink temperature (millidegrees C)

Each list of statements is ORed together, the condition is met if any of them are true.
